<?php

$container['db'] = function ($container) {
    $db = $container['settings']['database'];
    $dsn = 'mysql:host=' . $db['host'] . ';dbname=' . $db['database'] . ';charset=' . $db['charset'];
    return new Slim\PDO\Database($dsn, $db['username'], $db['password']);
};

$container['argon'] = function () {
    return new App\Utils\Argon;
};

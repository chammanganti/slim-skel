<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;

class Controller
{
    protected $db;
    protected $argon;

    public function __construct(ContainerInterface $container)
    {
        $this->db = $container->get('db');
        $this->argon = $container->get('argon');
    }
}

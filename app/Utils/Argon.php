<?php

namespace App\Utils;

class Argon
{
    /*
     * Hashes plain text
     *
     * @param $txt = plain text
     * returns hashed text
     */
    public function make($txt)
    {
        return password_hash($txt, PASSWORD_ARGON2I);
    }

    /*
     * Verifies hash
     *
     * @param $plain = plain text
     * @param $hash = hashed text
     * returns boolean
     */
    public function verify($plain, $hash)
    {
        return password_verify($plain, $hash);
    }
}

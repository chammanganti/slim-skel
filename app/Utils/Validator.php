<?php

namespace App\Utils;

class Validator
{
    /*
     *  Array of errors
     */
    private $errors = [];

    /*
     *  Error messages
     */
    private $errorMessages;

    /*
     *  Validates requests
     * 
     *  @param $request = ServerRequestInterface
     *  @param $keyRules = array of key and rules
     */
    public function validate($request, $keyRules)
    {
        foreach ($keyRules as $key => $rules) {
            $rules = explode('|', $rules);

            foreach ($rules as $rule) {
                if ($rule == 'required') {
                    $res = $this->requiredRule($request->getParam($key));
                    if ($res) {
                        $this->errors[$key][] = $res;
                    }
                }
            }
        }

        if (!empty($this->errors)) {
            $this->errorMessages = json_encode(['errors' => $this->errors]);
        }
    }

    /*
     *  Check if validation failed
     * 
     *  returns boolean
     */
    public function failed()
    {
        return !empty($this->errors) ? true : false;
    }

    /*
     *  Returns error messages
     * 
     *  returns error messages
     */
    public function errors()
    {
        return $this->errorMessages;
    }

    /*
     *  Verify if param is empty
     * 
     *  returns corresponding error message
     */
    private function requiredRule($param)
    {
        return !empty($param) ? null : 'This field is required.';
    }
}

<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {

}

date_default_timezone_set(getenv('TZ'));

$app = new Slim\App([
    'settings' => [
        'displayErrorDetails' => getenv('APP_DEBUG') === 'true',

        'app' => [
            'name' => getenv('APP_NAME')
        ],

        'database' => [
            'host' => getenv('DB_HOST'),
            'database' => getenv('DB_NAME'),
            'charset' => getenv('DB_CHARSET'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASS')
        ]
    ]
]);

$container = $app->getContainer();

require_once __DIR__ . '/../app/dependencies.php';
require_once __DIR__ . '/../routes/web.php';

$app->run();
